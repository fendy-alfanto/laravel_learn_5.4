<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('regis', function () {
    return view('v_regis_add');
});

Route::post('regis/add', 'RegisController@store');

//buat lihat inputan dari form
// Route::post('regis/add', function()
// {
//     var_dump($_POST);
// });
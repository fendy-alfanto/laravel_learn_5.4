<?php

namespace App\Http\Controllers;

use App\regis;
use Illuminate\Http\Request;
use DB;

class RegisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = $request->input('bld_username');
        $email = $request->input('bld_email');
        $password = $request->input('bld_password');

        // input satu per satu data ke DB
        DB::table('regis')->insert(
            ['username' => $name, 'email' => $email, 'password' => $password]
        );

        // input banyak data sekaligus ke DB
        // DB::table('regis')->insert([
        //     ['username' => 'jajal', 'email' => 'jajal', 'password' => 'jajal'],
        //     ['username' => 'jajal2', 'email' => 'jajal2', 'password' => 'jajal2']

        // ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\regis  $regis
     * @return \Illuminate\Http\Response
     */
    public function show(regis $regis)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\regis  $regis
     * @return \Illuminate\Http\Response
     */
    public function edit(regis $regis)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\regis  $regis
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, regis $regis)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\regis  $regis
     * @return \Illuminate\Http\Response
     */
    public function destroy(regis $regis)
    {
        //
    }
}
